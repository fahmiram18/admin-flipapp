-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 31, 2019 at 12:33 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `flipapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `app`
--

CREATE TABLE `app` (
  `app_id` int(5) NOT NULL,
  `app_name` varchar(50) NOT NULL,
  `app_dev` varchar(50) NOT NULL,
  `app_desc` varchar(2000) NOT NULL,
  `avg_rev` float NOT NULL,
  `app_foto` varchar(255) NOT NULL DEFAULT 'default.jpg',
  `app_harga` int(20) NOT NULL,
  `id_category` int(11) NOT NULL,
  `app_foto1` varchar(255) NOT NULL DEFAULT 'default.jpg',
  `app_foto2` varchar(255) NOT NULL DEFAULT 'default.jpg',
  `app_foto3` varchar(255) NOT NULL DEFAULT 'default.jpg',
  `user_id` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app`
--

INSERT INTO `app` (`app_id`, `app_name`, `app_dev`, `app_desc`, `avg_rev`, `app_foto`, `app_harga`, `id_category`, `app_foto1`, `app_foto2`, `app_foto3`, `user_id`) VALUES
(1, 'Jetset', 'Sumotec (Pvt.) Limited', 'Pakistans largest travel agency, Sonya Southern Travels Network, is proud to introduce our latest product, Jetset.pk. We guarantee that you will get the best deals on every major carrier, be it through your local travel agent, or any other local websites! Found a cheaper price? Jetset.pk guarantees that we will beat it!', 0, 'default.jpg', 0, 0, 'default.jpg', 'default.jpg', 'default.jpg', ''),
(2, 'Trello—Organize anything with anyone, anywhere!', 'Trello, Inc.', 'Trello gives you perspective over all your projects, at work and at home.', 4.66, 'default.jpg', 0, 0, 'default.jpg', 'default.jpg', 'default.jpg', ''),
(6, 'Aset App', 'Joice Aurellia', 'Aplikasi management Assets', 0, 'default.jpg', 0, 0, 'default.jpg', 'default.jpg', 'default.jpg', ''),
(8, 'Puskesmas Keliling', 'Samuel', 'Aplikasi sistem informasi puskesmas', 4, 'default.jpg', 0, 0, 'default.jpg', 'default.jpg', 'default.jpg', ''),
(9, '123', 'felix harold', '123', 0, 'default.jpg', 123, 0, 'default.jpg', 'default.jpg', 'default.jpg', ''),
(10, 'ds', 'felix harold', 'ds', 0, '.png', 12, 0, 'default.jpg', 'default.jpg', 'default.jpg', ''),
(11, 'dsad', 'felix harold', 'dsad', 0, '.png', 12, 0, 'default.jpg', 'default.jpg', 'default.jpg', ''),
(12, 'das', 'felix harold', '3123', 0, '.png', 123, 0, 'default.jpg', 'default.jpg', 'default.jpg', ''),
(63, 'Rumah Sakit', 'felix harold', 'Barang langkah nih', 0, '.jpg', 1000, 3, 'default.jpg', 'default.jpg', 'default.jpg', ''),
(64, 'Website Hutan', 'felix harold', 'website baru nich', 0, '2.jpg', 1000000, 1, 'default.jpg', 'default.jpg', 'default.jpg', '6');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `category_name`) VALUES
(1, 'Barang Ghaib'),
(2, 'Contacts'),
(3, 'Location'),
(4, 'Photos/Media/Files'),
(5, 'Storage'),
(11, 'Android/IOS'),
(12, 'HTML CSS2');

-- --------------------------------------------------------

--
-- Table structure for table `checkout`
--

CREATE TABLE `checkout` (
  `checkout_token` varchar(20) NOT NULL,
  `app_id` int(11) NOT NULL,
  `user_id` varchar(128) NOT NULL,
  `username` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `address` varchar(256) NOT NULL,
  `ship_method` varchar(20) NOT NULL,
  `ship_date` date NOT NULL,
  `comment` varchar(512) NOT NULL,
  `phone` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `checkout`
--

INSERT INTO `checkout` (`checkout_token`, `app_id`, `user_id`, `username`, `email`, `address`, `ship_method`, `ship_date`, `comment`, `phone`) VALUES
('DoLi9wM9N', 8, '20', 'Stevanus', 'steve@gmail.com', 'Melwood st.22', 'E-Mail', '2019-07-06', 'woww', '82277338067'),
('fCCTHme2W', 8, '20', 'Mo Salah', 'salah@gmail.com', 'Melwood st.21', 'E-Mail', '2019-06-01', '  ggggggggg', '82277338066'),
('LXmQrPf42', 2, '20', '', 'manuruasdsada@gmail.com', 'dasd\r\nasd', 'E-Mail', '2000-02-01', 'asdasd', '082277668894'),
('VMvGKOjKg', 8, '20', 'sammm', 'asdklalml@gmail.com', 'asdsadasd', 'E-Mail', '2018-12-30', 'asdasda', '090312908'),
('YMqwSHIA9', 63, '6', 'joshpn', 'joshuapanjaitan01@hotmail.com', '-\r\n-', 'E-Mail', '2019-05-25', '  Mainkan', '82277338066');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id_comment` int(11) NOT NULL,
  `date_comment` int(11) NOT NULL,
  `username_comment` varchar(100) NOT NULL,
  `user_id_comment` varchar(100) NOT NULL,
  `comments` varchar(1000) NOT NULL,
  `app_id` int(11) NOT NULL,
  `reply_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id_comment`, `date_comment`, `username_comment`, `user_id_comment`, `comments`, `app_id`, `reply_id`) VALUES
(22, 0, '0', '0', '0', 0, 0),
(24, 1558595904, 'Joshua Panjaitan', '103419611834344490015', 'hi penjual!', 6, 0),
(25, 1558596924, 'felix harold', '6', 'Coba ', 8, 0),
(28, 1558670724, 'Joice Aurellia', '4', 'siapa des', 8, 0),
(29, 1559295250, 'felix harold', '6', 'stok ada ?', 63, 0);

-- --------------------------------------------------------

--
-- Table structure for table `info`
--

CREATE TABLE `info` (
  `info_id` int(5) NOT NULL,
  `app_id` int(5) NOT NULL,
  `info_version` varchar(20) NOT NULL,
  `info_requirement` varchar(20) NOT NULL,
  `info_last_update` varchar(20) NOT NULL,
  `info_downloaded` varchar(20) NOT NULL,
  `info_size` varchar(20) NOT NULL,
  `info_purchase` varchar(20) NOT NULL,
  `info_dev` varchar(50) NOT NULL,
  `info_release` varchar(20) NOT NULL,
  `info_age` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `info`
--

INSERT INTO `info` (`info_id`, `app_id`, `info_version`, `info_requirement`, `info_last_update`, `info_downloaded`, `info_size`, `info_purchase`, `info_dev`, `info_release`, `info_age`) VALUES
(1, 1, '1.1\r\n', '4.0.3 and up', 'January 10, 2019', '50+', '2.0M', '', 'Sumotec (Pvt.) Limited', '', '3+'),
(2, 2, 'Varies with device', 'Varies with device', 'April 1, 2019\r\n', '5,000,000+', 'Varies with device', '', 'support@trello.com', '', 'Rated for 3+'),
(4, 6, 'Varies with device', 'Varies with device', 'March 27, 2019\r\n', '10,000,000+', 'Varies with device', '', 'feedback@slack.com', '', 'Rated for 3+'),
(5, 8, 'Varies with device', 'Varies with device', 'April 4, 2019\r\n', '1,000,000+', 'Varies with device', '', 'android-help@microsoft.com', '', 'Rated for 3+'),
(10, 8, '1.2', 'Varies with device', 'April 1,2019', '10+', '3.0M', ' ', 'info@samuel.uk', '', '10+');

-- --------------------------------------------------------

--
-- Table structure for table `keys`
--

CREATE TABLE `keys` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT '0',
  `is_private_key` tinyint(1) NOT NULL DEFAULT '0',
  `ip_addresses` text,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `keys`
--

INSERT INTO `keys` (`id`, `user_id`, `key`, `level`, `ignore_limits`, `is_private_key`, `ip_addresses`, `date_created`) VALUES
(1, 1, 'flipp123', 1, 0, 0, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE `permission` (
  `permission_id` int(5) NOT NULL,
  `info_id` int(5) NOT NULL,
  `permission_detail` varchar(100) NOT NULL,
  `category_id` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permission`
--

INSERT INTO `permission` (`permission_id`, `info_id`, `permission_detail`, `category_id`) VALUES
(1, 1, 'view network connections', 6),
(2, 1, 'full network access', 6),
(3, 2, 'find accounts on the device', 1),
(4, 2, 'add or remove accounts', 1),
(5, 2, 'find accounts on the device', 2),
(6, 2, 'precise location (GPS and network-based)', 3),
(7, 2, 'read the contents of your USB storage', 4),
(8, 2, 'read the contents of your USB storage', 5),
(9, 2, 'receive data from Internet', 6),
(10, 2, 'view network connections', 6),
(11, 2, 'create accounts and set passwords', 6),
(12, 2, 'full network access', 6),
(13, 2, 'read sync settings', 6),
(14, 2, 'use accounts on the device', 6),
(15, 2, 'control vibration', 6),
(16, 2, 'prevent device from sleeping', 6),
(17, 2, 'toggle sync on and off', 6),
(18, 3, 'find accounts on the device', 1),
(19, 3, 'find accounts on the device', 2),
(20, 3, 'read your contacts', 2),
(21, 3, 'read phone status and identity', 7),
(22, 3, 'read the contents of your USB storage', 4),
(23, 3, 'modify or delete the contents of your USB storage', 4),
(24, 3, 'read the contents of your USB storage', 5),
(25, 3, 'modify or delete the contents of your USB storage', 5),
(26, 3, 'record audio', 0),
(27, 3, 'read phone status and identity', 8),
(28, 3, 'receive data from Internet', 6),
(29, 3, 'view network connections', 6),
(30, 3, 'pair with Bluetooth devices', 6),
(31, 3, 'full network access', 6),
(32, 3, 'change your audio settings', 6),
(33, 3, 'run at startup', 6),
(34, 3, 'control vibration', 6),
(35, 3, 'prevent device from sleeping', 6),
(44, 4, 'find accounts on the device', 1),
(45, 4, 'add or remove accounts', 1),
(46, 4, 'find accounts on the device', 2),
(47, 4, 'read your contacts', 2),
(48, 4, 'modify your contacts', 2),
(49, 4, 'read the contents of your USB storage', 4),
(50, 4, 'modify or delete the contents of your USB storage', 4),
(51, 4, 'read the contents of your USB storage', 5),
(52, 4, 'modify or delete the contents of your USB storage', 5),
(53, 4, 'view Wi-Fi connections', 9),
(54, 4, 'read calendar events plus confidential information', 10),
(55, 4, 'receive data from Internet', 6),
(56, 4, 'view network connections', 6),
(57, 4, 'full network access', 6),
(58, 4, 'use accounts on the device', 0),
(59, 4, 'prevent device from sleeping', 6),
(60, 5, 'retrieve running apps', 0),
(61, 5, 'find accounts on the device', 1),
(62, 5, 'read your own contact card', 1),
(63, 5, 'read calendar events plus confidential information', 10),
(64, 5, 'find accounts on the device', 2),
(65, 5, 'read your contacts', 2),
(66, 5, 'approximate location (network-based)', 3),
(67, 5, 'precise location (GPS and network-based)', 3),
(68, 5, 'read phone status and identity', 7),
(69, 5, 'read the contents of your USB storage', 4),
(70, 5, 'modify or delete the contents of your USB storage', 4),
(71, 5, 'read the contents of your USB storage', 5),
(72, 5, 'modify or delete the contents of your USB storage', 5),
(73, 5, 'take pictures and videos', 0),
(74, 5, 'view Wi-Fi connections', 9),
(75, 5, 'read phone status and identity', 8),
(76, 5, 'Access download manager.', 6),
(77, 5, 'download files without notification', 6),
(78, 5, 'receive data from Internet', 6),
(79, 5, 'view network connections', 6),
(80, 5, 'pair with Bluetooth devices', 6),
(81, 5, 'access Bluetooth settings', 6),
(82, 5, 'change network connectivity', 6),
(83, 5, 'disable your screen lock', 6),
(84, 5, 'expand/collapse status bar', 6),
(85, 5, 'full network access', 6),
(86, 5, 'run at startup', 6),
(87, 5, 'control vibration', 6),
(88, 5, 'prevent device from sleeping', 6),
(89, 5, 'modify system settings', 6),
(90, 5, 'view network connections', 6),
(91, 5, 'pair with Bluetooth devices', 6),
(92, 5, 'access Bluetooth settings', 6),
(93, 5, 'send sticky broadcast', 6),
(94, 5, 'change network connectivity', 6),
(95, 5, 'disable your screen lock', 6),
(96, 5, 'expand/collapse status bar', 6),
(97, 5, 'full network access', 6),
(98, 5, 'run at startup', 6),
(99, 5, 'control vibration', 6),
(100, 5, 'prevent device from sleeping', 5),
(101, 5, 'modify system settings', 6);

-- --------------------------------------------------------

--
-- Table structure for table `reply`
--

CREATE TABLE `reply` (
  `comment_id` int(11) NOT NULL,
  `reply_id` int(11) NOT NULL,
  `username_reply` varchar(100) NOT NULL,
  `reply` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reply`
--

INSERT INTO `reply` (`comment_id`, `reply_id`, `username_reply`, `reply`) VALUES
(0, 23, 'Inject', '-g');

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `review_id` int(5) NOT NULL,
  `app_id` int(5) NOT NULL,
  `review_name` varchar(50) NOT NULL,
  `review_date` int(20) NOT NULL,
  `review_desc` varchar(500) NOT NULL,
  `review_rating` int(1) NOT NULL,
  `review_like` int(5) NOT NULL,
  `user_id` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`review_id`, `app_id`, `review_name`, `review_date`, `review_desc`, `review_rating`, `review_like`, `user_id`) VALUES
(2, 2, 'Brett West', 2019, 'Amazing. The more you use it, the more familiar, the more you exploit it\'s fluid operation and power ups, the more it becomes the primary source for all things home, business and travel. Our team love it. There are limitations though and an annoying problem where some cards don\'t sync, so i uninstall from phone and reinstall app to continue.', 4, 5, '0'),
(3, 2, 'Hoshi Inoue', 2019, 'thank you for adding powerful filter and labeling abilities. The only current annoyance is that attached images get clipped. I want the images to shrink to fit the 4x3 display window. Cutting the images off makes a whole lot more work for people.', 5, 9, '0'),
(11, 8, 'Vilareal', 2019, 'App nya kok nge lag ya ?', 5, 1, '0'),
(13, 8, 'Manuel', 14, 'Siap!!', 3, 1, '0'),
(14, 8, 'Manuel', 15, 'Test 1', 4, 1, '0'),
(39, 2, 'Joice Aurellia', 23, 'saya suka, saya suka', 5, 1, '4'),
(43, 2, 'Joshua Panjaitan', 1558947085, 'Gutten Nacht!', 5, 1, '103419611834344490015');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `role_id` int(5) NOT NULL,
  `app_id` int(5) NOT NULL,
  `app_role` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`role_id`, `app_id`, `app_role`) VALUES
(1, 1, 'Travel & Local'),
(2, 2, 'Productivity'),
(3, 3, 'Business'),
(4, 4, 'Business'),
(5, 5, 'Business');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `image` varchar(128) NOT NULL,
  `password` varchar(256) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_active` int(1) NOT NULL,
  `date_created` int(11) NOT NULL,
  `via` varchar(128) NOT NULL,
  `company` varchar(30) NOT NULL,
  `address` varchar(30) NOT NULL,
  `address2` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `state` varchar(30) NOT NULL,
  `zip` varchar(20) NOT NULL,
  `credit` varchar(20) NOT NULL,
  `spec1` varchar(15) NOT NULL,
  `spec2` varchar(15) NOT NULL,
  `spec3` varchar(15) NOT NULL,
  `about` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `image`, `password`, `role_id`, `is_active`, `date_created`, `via`, `company`, `address`, `address2`, `city`, `state`, `zip`, `credit`, `spec1`, `spec2`, `spec3`, `about`) VALUES
(5, 'joshua panjaitan', 'joshuapanjaitan01@gmail.com', 'default.jpg', '$2y$10$rCmN8raF6315EC48NAazVelhANHOQKNjav42jH.uDx02JwxyZHMOa', 1, 1, 1556867629, 'manual', '', '', '', '', '', '', '', '', '', '', ''),
(6, 'felix harold', 'felixharold@outlook.com', 'default.jpg', '$2y$10$z.kuiMySw9Us8vUCm3uYQutaS6R300lcRQmSFX0W8WiqS9e9WPFIm', 2, 1, 1556941763, 'manual', 'Liverpool', '-, -', '-', 'Liverpool', '-', '29012', '', '', '', '', ''),
(7, 'Mo Salah', 'salah@gmail.com', 'featuredimage-13-600x4051.jpg', '$2y$10$zHO5ac6INZRVSwwzn5jjz.tiKSmhf.jhqsvvQIe.xbBxW4n3VSg9u', 3, 1, 1556970276, 'manual', 'Liverpool', 'st.Melwood no.21', '', 'Liverpool', 'England', '29012', '551', '', '', '', ''),
(8, 'tasya simanjuntak', 'tasya@gmail.com', 'default.jpg', '$2y$10$8hyJ4.s9EHmV02C8oGGoEuG6ivKbHLYPLgLqRwFrelx/4iAq8TSB6', 3, 0, 1557041286, 'manual', '', '', '', '', '', '', '', '', '', '', ''),
(12, 'cocot3', 'joiceacp01@gmail.com', 'default.jpg', '$2y$10$8hHQuYUavtICSsVtU0K1O.hee9XNZYRjVGn1OK8TSywB6XYGQNpuG', 3, 1, 1557045077, 'manual', 'PT. Baru', 'jln. satu', 'jln. dua', 'bandung', 'Indonesia', '1233', '', '', '', '', ''),
(13, 'Joice Aja', 'joiceacp02@gmail.com', 'default.jpg', '$2y$10$7JqKE3zgtXwKuZLb0ZDvmu/ZTEbLqgaaBNQ7khPhFLhb9rsG.3Nda', 3, 1, 1557046883, 'manual', '', '', '', '', '', '', '', '', '', '', ''),
(16, 'Joice Dev', 'joiceacp03@gmail.com', 'default.jpg', '$2y$10$azuXfL.M1WmFgYtjfxdxLOh65OlD.Qr8KRI2WhOS7SZzLZJivAtkK', 2, 1, 1557049934, 'manual', '', '', '', '', '', '', '', '', '', '', ''),
(19, 'santos', 'santos@gmail.com', 'default.jpg', '$2y$10$is7uQPIi24Favx9NarJQrOYsVDQvbu8', 3, 1, 1557979026, 'manual', '', '', '', '', '', '', '', '', '', '', ''),
(20, 'sams', 'samuelmanurung.st@gmail.com', 'featuredimage-13-600x405.jpg', '$2y$10$c7FUop0j2zGACp9pkOv2tuhpfe9Nw5L7v8icYtDfQCVYzKwevofUe', 2, 1, 1558408266, 'manual', 'sammm', 'sukabiru', 'jakartas', 'bandungs', 'indonesia ', '123', '12312', 'java', 'java', 'java', 'tampan');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `role`) VALUES
(1, 'Admin'),
(2, 'Developer'),
(3, 'Customer');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `app`
--
ALTER TABLE `app`
  ADD PRIMARY KEY (`app_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `checkout`
--
ALTER TABLE `checkout`
  ADD PRIMARY KEY (`checkout_token`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id_comment`);

--
-- Indexes for table `info`
--
ALTER TABLE `info`
  ADD PRIMARY KEY (`info_id`);

--
-- Indexes for table `keys`
--
ALTER TABLE `keys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`permission_id`);

--
-- Indexes for table `reply`
--
ALTER TABLE `reply`
  ADD PRIMARY KEY (`reply_id`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`review_id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `app`
--
ALTER TABLE `app`
  MODIFY `app_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id_comment` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `info`
--
ALTER TABLE `info`
  MODIFY `info_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `keys`
--
ALTER TABLE `keys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `permission`
--
ALTER TABLE `permission`
  MODIFY `permission_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `reply`
--
ALTER TABLE `reply`
  MODIFY `reply_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `review_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `role_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
