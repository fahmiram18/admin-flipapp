$('.blas').after('<div><ul class="pagination odin"id="nav"></ul></div>');
var rowsShown = 10; //jumlah baris yang ingin ditampilkan
var rowsTotal = $('#table tbody tr').length;
var numPages = rowsTotal / rowsShown;
for (i = 0; i < numPages; i++) {
	var pageNum = i + 1;
	$('#nav').append('<li class="page-item"><a class="page-link" href="#" rel="' + i + '">' + pageNum + '</a></li> ');
}
$('#table tbody tr').hide();
$('#table tbody tr').slice(0, rowsShown).show();
$('#nav li:first').addClass('active');
$('#nav li a').bind('click', function () {
	$('#nav li').removeClass('active');
	$(this).parent().addClass('active');
	var currPage = $(this).attr('rel');
	var startItem = currPage * rowsShown;
	var endItem = startItem + rowsShown;
	$('#table tbody tr').css('opacity', '1').hide().slice(startItem, endItem).css('display', 'table-row');
});
