<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Portfolio_model extends CI_Model
{

    private $table_name = 'apps';

    public function get()
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->select('
            app_id,
            users.user_email,
            app_status.app_status,
            apps.app_status_id,
            app_name,
            app_demo_link,
            app_requirement,
            app_version
		');
        $this->db->from($this->table_name);
        $this->db->join('users', 'users.user_id = apps.user_id');
        $this->db->join('app_status', 'app_status.app_status_id = apps.app_status_id');
        $result = $this->db->get()->result_array();
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public function update($id, $status)
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->where('app_id', $id);
        $this->db->update($this->table_name, ['app_status_id' => $status]);
        $result = $this->db->affected_rows();
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return $result;
        } else {
            return false;
        }
    }
}

/* End of file Portfolio_model.php */
