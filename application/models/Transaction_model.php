<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaction_model extends CI_Model
{

    private $table_name = 'transactions';

    public function getAll()
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->select('
            transactions.checkout_token,
            users.user_email,
            shipping_methods.shipping_method,
            last_updated,
            transactions_status.status
		');
        $this->db->from($this->table_name);
        $this->db->join('users', 'users.user_id = transactions.user_id');
        $this->db->join('shipping_methods', 'shipping_methods.shipping_method_id = transactions.shipping_method_id');
        $this->db->join('transactions_status', 'transactions_status.transaction_status_id = transactions.transaction_status_id');
        $result = $this->db->get()->result_array();
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getDetail() {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->select('
            transactions.checkout_token,
            apps.app_name,
            total_price
		');
        $this->db->from($this->table_name);
        $this->db->join('transactions_history', 'transactions.checkout_token = transactions_history.checkout_token');
        $this->db->join('apps', 'transactions_history.app_id = apps.app_id');
        $result = $this->db->get()->result_array();
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public function update($token, $status)
    {
        $this->db->trans_start();
        $this->db->trans_strict(FALSE);
        $this->db->where('checkout_token', $token);
        $this->db->update($this->table_name, ['transaction_status_id' => 2]);
        $result = $this->db->affected_rows();
        $this->db->trans_complete();

        if ($this->db->trans_status()) {
            return $result;
        } else {
            return false;
        }
    }
}

/* End of file ModelName.php */
