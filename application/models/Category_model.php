<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Category_model extends CI_Model
{

  private $table_name = 'categories';

  public function getAll() {
    $this->db->trans_start();
    $this->db->trans_strict(FALSE);
    $this->db->from($this->table_name);
    $result = $this->db->get()->result_array();
    $this->db->trans_complete();

    if ($this->db->trans_status()) {
			return $result;
		} else {
			return FALSE;
		}
  }

  public function getById($id)
  {
    $this->db->trans_start();
    $this->db->trans_strict(FALSE);
    $this->db->from($this->table_name);
    $this->db->where('category_id', $id);
    $result = $this->db->get()->result_array();
    $this->db->trans_complete();

    if ($this->db->trans_status()) {
      if (count($result) == 0) {
        return 0;
      } else {
        return $result[0];
      }
    } else {
      return FALSE;
    }
  }

  public function insert($category)
  {
    $this->db->trans_start();
    $this->db->trans_strict(FALSE);
    $this->db->insert($this->table_name, $category);
    $result = $this->db->affected_rows();
    $this->db->trans_complete();

    if ($this->db->trans_status()) {
      return $result;
    } else {
      return FALSE;
    }
  }

  public function update($categories, $id)
  {
    $this->db->trans_start();
    $this->db->trans_strict(FALSE);
    $this->db->where('category_id', $id);
    $this->db->update($this->table_name, $categories);
    $result = $this->db->affected_rows();
    $this->db->trans_complete();

    if ($this->db->trans_status()) {
      return $result;
    } else {
      return false;
    }
  }

  public function delete($id)
  {
    $this->db->trans_start();
    $this->db->trans_strict(FALSE);
    $this->db->where('category_id', $id);
    $this->db->delete($this->table_name);
    $result = $this->db->affected_rows();
    $this->db->trans_complete();

    if ($this->db->trans_status()) {
      return $result;
    } else {
      return false;
    }
  }
}

/* End of file ModelName.php */
