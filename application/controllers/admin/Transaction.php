<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('transaction_model', 'tran_model');
    }

    public function index()
    {
        $data['judul'] = 'FlipApp Admin';
        $data['transaction'] = $this->tran_model->getAll();
        $data['detail'] = $this->tran_model->getDetail();

        $this->load->view('templates/header', $data);
        $this->load->view('admin/transaction/transaction', $data);
        $this->load->view('templates/footer');
    }

    public function updateStatus($token) {

        $status = 2;

        $update = $this->tran_model->update($token, $status);

        if ($update) {
            $this->session->set_flashdata('working', 'Data berhasil diupdate');
        } else {
            $this->session->set_flashdata('not_working', 'Data tidak berhasil diupdate');
        }
        redirect('admin/transaction');
    }

}

/* End of file Controllername.php */
