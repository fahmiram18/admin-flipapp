<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Portfolio extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('portfolio_model', 'port_model');
    }

    public function index()
    {
        $data['judul'] = 'FlipApp Admin';
        $data['portfolio'] = $this->port_model->get();

        $this->load->view('templates/header', $data);
        $this->load->view('admin/portfolio/portfolio', $data);
        $this->load->view('templates/footer');
    }

    public function updateStatus($id, $status) {
        $update = $this->port_model->update($id, $status);

        if ($update) {
            $this->session->set_flashdata('working', 'Data berhasil diupdate');
        } else {
            $this->session->set_flashdata('not_working', 'Data tidak berhasil diupdate');
        }
        redirect('admin/portfolio');
    }

}

/* End of file Portfolio.php */
