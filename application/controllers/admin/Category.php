<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Category extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('category_model', 'cat_model');
    }

    public function index()
    {
        $data['judul'] = 'FlipApp Admin';
        $data['categories'] = $this->cat_model->getAll();

        $this->load->view('templates/header', $data);
        $this->load->view('admin/category/categories');
        $this->load->view('templates/footer');
    }

    public function createCategory()
    {
        $this->form_validation->set_rules('category_name', 'Category Name', 'required|trim');

        if (!$this->form_validation->run()) {
            $data['judul'] = 'FlipApp Admin';

            $this->load->view('templates/header', $data);
            $this->load->view('admin/category/form_create');
            $this->load->view('templates/footer');
        } else if ($this->form_validation->run()) {

            $category = [
                'category_id' => '',
                'category_name' => $this->input->post('category_name')
            ];

            $insert = $this->cat_model->insert($category);

            if ($insert) {
                $this->session->set_flashdata('working', 'Data berhasil diinsert');
            } else {
                $this->session->set_flashdata('not_working', 'Data tidak bisa diinsert');
            }
            redirect('admin/category');
        } else { }
    }

    public function changeCategory($id)
    {
        $this->form_validation->set_rules('category_name', 'Category Name', 'required|trim');

        if (!$this->form_validation->run()) {
            $data['judul'] = 'FlipApp Admin';
            $data['category'] = $this->cat_model->getById($id);

            $this->load->view('templates/header', $data);
            $this->load->view('admin/category/form_update', $data);
            $this->load->view('templates/footer');
        } else if ($this->form_validation->run()) {

            $category = [
                'category_id' => $id,
                'category_name' => $this->input->post('category_name')
            ];

            $update = $this->cat_model->update($category, $id);

            if ($update) {
                $this->session->set_flashdata('working', 'Data berhasil diubah');
            } else {
                $this->session->set_flashdata('not_working', 'Data tidak bisa diubah');
            }
            redirect('admin/category');
        } else { }
    }

    public function deleteCategory($id)
    {
        $status = $this->cat_model->delete($id);
        if ($status) {
            $this->session->set_flashdata('working', 'Data berhasil di hapus!');
        } else {
            $this->session->set_flashdata('not_working', 'Data tidak bisa dihapus');
        }
        redirect('admin/category');
    }
}

    /* End of file Controllername.php */
