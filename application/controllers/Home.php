<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function index(){
        $data['judul'] = 'FlipApp Admin';

        $this->load->view('templates/header',$data);
        $this->load->view('admin/home');
        $this->load->view('templates/footer');
    }

}

/* End of file Controllername.php */
