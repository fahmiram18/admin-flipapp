<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $judul?></title>
    <style>
        h1{
            text-align : center;
            margin-top:10%;
        }
        .max{
            margin-left: 45%;
        }
    </style>
</head>
<body>
    <h1>Error 404 Page Not Found</h1>
    <a href="<?= base_url('')?>" class="max"> > Back to Home</a>
</body>
</html>