<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <?php if ($this->session->flashdata('working')) : ?>
        <div class="alert alert-success alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h4><i class="icon fa fa-check"></i> Alert!</h4>
          <?= $this->session->flashdata('working'); ?>
        </div>

        <?php elseif ($this->session->flashdata('not_working')) : ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h4><i class="icon fa fa-ban"></i> Alert!</h4>
          <?= $this->session->flashdata('not_working'); ?>
        </div>
        <?php endif; ?>
        <div class="box">
          <div class="box-header">
            <div class="row">
              <div class="col-xs-12">
                <h3 class="box-title">List Of Transactions</h3>
              </div>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="example2" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Checkout Token</th>
                  <th>User Email</th>
                  <th>Shipping Method</th>
                  <th>Time</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; ?>
                <?php foreach ($transaction as $t) : ?>
                <tr>
                  <td><?= $i ?></td>
                  <td><?= $t['checkout_token'] ?></td>
                  <td><?= $t['user_email'] ?></td>
                  <td><?= $t['shipping_method'] ?>
                  <td><?= $t['last_updated'] ?></td>
                  <?php if ($t['status'] == 'complete') : ?>
                  <td><?= $t['status'] ?></td>
                  <?php else : ?>
                  <td><a href="<?= site_url('admin/transaction/updateStatus/') . $t['checkout_token'] ?>" class="btn btn-xs btn-warning"><?= $t['status'] ?></a></td>
                  <?php endif ?>
                  <td><button class="btn btn-xs btn-default" data-toggle="modal" data-target="#modal-detail-<?= $i ?>">Detail</a></td>
                </tr>
                <div class="modal fade" id="modal-detail-<?= $i ?>" role="dialog">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Detail Order <?= $t['checkout_token'] ?></h4>
                      </div>
                      <div class="modal-body">
                        <ul class="list-group">
                          <?php $i = 1; ?>
                          <?php foreach ($detail as $d) : ?>
                          <?php if ($d['checkout_token'] == $t['checkout_token']) : ?>
                          <li class="list-group-item">
                            <div class="row">
                              <div class="col-xs-6">
                                <?= $d['app_name'] ?>
                              </div>
                              <div class="col-xs-6">
                                <div class="pull-right">
                                  Rp <?= number_format($d['total_price'], 2, ',', '.') ?>
                                </div>
                              </div>
                            </div>
                          </li>
                          <?php endif ?>
                          <?php $i++; ?>
                          <?php endforeach ?>
                        </ul>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">OK</button>
                      </div>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                </div>
                <?php $i++; ?>
                <?php endforeach ?>
              </tbody>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>