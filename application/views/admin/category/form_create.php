<div class="content-wrapper">
    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Category</h3>
            </div>
            <!-- /.box-header -->
            <form method="post" action="<?= site_url('admin/category/createCategory') ?>">
                <div class="box-body">
                    <div class="form-group">
                        <label>Category Name</label>
                        <input type="text" class="form-control" id="category_name" placeholder="Enter ..." name="category_name" value="<?= set_value('category_name') ?>">
                        <?= form_error('category_name', '<small class="text-danger">', '</small>') ?>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
            <!-- /.box-footer -->
        </div>
    </section>
</div>