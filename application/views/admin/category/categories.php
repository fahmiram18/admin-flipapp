<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">

        <?php if ($this->session->flashdata('working')) : ?>
        <div class="alert alert-success alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h4><i class="icon fa fa-check"></i> Alert!</h4>
          <?= $this->session->flashdata('working'); ?>
        </div>

        <?php elseif ($this->session->flashdata('not_working')) : ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h4><i class="icon fa fa-ban"></i> Alert!</h4>
          <?= $this->session->flashdata('not_working'); ?>
        </div>
        <?php endif; ?>

        <div class="box">
          <div class="box-header">
            <div class="row">
              <div class="col-xs-6">
                <h3 class="box-title">List Of Categories</h3>
              </div>
              <div class="col-xs-6">
                <div class="pull-right">
                  <a href="<?= site_url('admin/category/createCategory') ?>">
                    <button type="button" class="btn btn-primary">Create Category</button>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="example2" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Category Name</th>
                  <th>Option</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; ?>
                <?php foreach ($categories as $c) : ?>
                <tr>
                  <td><?= $i ?></td>
                  <td><?= $c['category_name'] ?></td>
                  <td><a href="<?= site_url('admin/category/changeCategory/') . $c['category_id'] ?>" class="btn btn-xs btn-primary">Change</a> |
                    <a href="<?= site_url('admin/category/deleteCategory/') . $c['category_id'] ?>" class="btn btn-xs btn-danger">Delete</a></td>
                </tr>
                <?php $i++; ?>
                <?php endforeach ?>
              </tbody>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>