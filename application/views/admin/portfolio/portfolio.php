<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <?php if ($this->session->flashdata('working')) : ?>
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Alert!</h4>
                    <?= $this->session->flashdata('working'); ?>
                </div>

                <?php elseif ($this->session->flashdata('not_working')) : ?>
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                    <?= $this->session->flashdata('not_working'); ?>
                </div>
                <?php endif; ?>
                <div class="box">
                    <div class="box-header">
                        <div class="row">
                            <div class="col-xs-12">
                                <h3 class="box-title">List Of Apps</h3>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>App Name</th>
                                    <th>Developer Email</th>
                                    <th>App Version</th>
                                    <th>App Requirement</th>
                                    <th>Status</th>
                                </tr>
                            <tbody>
                                <?php $i = 1; ?>
                                <?php foreach ($portfolio as $p) : ?>
                                <tr>
                                    <td><?= $i ?></td>
                                    <td><a href="<?= $p['app_demo_link'] ?>"><u><?= $p['app_name'] ?></u></a></td>
                                    <td><?= $p['user_email'] ?></td>
                                    <td><?= $p['app_version'] ?></td>
                                    <td><?= $p['app_requirement'] ?></td>
                                    <td>
                                        <div class="btn-group dropdown show">
                                            <?php if ($p['app_status_id'] == 1) : ?>
                                            <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <?= $p['app_status'] ?>
                                            </button>
                                            <?php elseif ($p['app_status_id'] == 2) : ?>
                                            <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <?= $p['app_status'] ?>
                                            </button>
                                            <?php elseif ($p['app_status_id'] == 3) : ?>
                                            <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <?= $p['app_status'] ?>
                                            </button>
                                            <?php endif ?>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item btn" href="<?= site_url('admin/portfolio/updateStatus/') . $p['app_id'] . '/1' ?>">Passed</a><br>
                                                <a class="dropdown-item btn" href="<?= site_url('admin/portfolio/updateStatus/') . $p['app_id'] . '/2' ?>">Hold</a><br>
                                                <a class="dropdown-item btn" href="<?= site_url('admin/portfolio/updateStatus/') . $p['app_id'] . '/3' ?>">Failed</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <?php $i++; ?>
                                <?php endforeach ?>
                            </tbody>
                            </thead>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>